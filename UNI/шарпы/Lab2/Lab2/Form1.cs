﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab2
{
    public partial class Form1 : Form
    {
        private int fieldWidth;
        private int fieldHigh;
        private Point curPos;
        private int amountExperiments;
        private double upProbability;
        private double downProbability;
        private double rightProbability;
        private double leftProbability;
        private double stopProbability;
       

        private int upAmount;
        private int downAmount;
        private int rightAmount;
        private int leftAmount;
        private int stopAmount;

        private int[][] stopArr;

        public Form1()
        {
            InitializeComponent();
      
        }


       
        
        public void init()
        {
        upAmount=0;
        downAmount=0;
        rightAmount=0;
        leftAmount=0;
        stopAmount=0;
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            chart2.Series[0].Points.Clear();
            chart2.Series[1].Points.Clear();
            chart3.Series.Clear();
        }

        private void initArr(int[] arr)
        {
            for(int i = 0; i < arr.Length; i++)
            {
                arr[i] = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            init();

            fieldWidth = Decimal.ToInt32(widthUpDown.Value);
            fieldHigh = Decimal.ToInt32(highUpDown.Value);
            if (Decimal.ToInt32(firstPosUpDown1.Value) >= Decimal.ToInt32(widthUpDown.Value))
            {
                MessageBox.Show("Defaul Х position is out of bounds");
                return;
            }
            if (Decimal.ToInt32(firstPosUpDown2.Value) >= Decimal.ToInt32(highUpDown.Value))
            {
                MessageBox.Show("Defaul Y position is out of bounds");
                return;
            }
            stopArr = new int[fieldHigh][];
            for(int i = 0; i < fieldHigh; i++)
            {
                stopArr[i] = new int[fieldWidth];
            }

            curPos = new Point(Decimal.ToInt32(firstPosUpDown1.Value), Decimal.ToInt32(firstPosUpDown2.Value));
            amountExperiments = Decimal.ToInt32(AmountExpUpDown.Value);

            upProbability = Decimal.ToDouble(numericUp.Value);
            downProbability = Decimal.ToDouble(numericDown.Value);
            rightProbability = Decimal.ToDouble(numericRight.Value);
            leftProbability = Decimal.ToDouble(numericLeft.Value);
            stopProbability = Decimal.ToDouble(numericStop.Value);

            double checkRe = Math.Round(upProbability + downProbability + rightProbability + leftProbability + stopProbability, 5);
            if (checkRe != 1.0)
            {
                MessageBox.Show("Posibilities sum is not equal to 1");
                return;
            }
            int[] upArr = new int[fieldWidth];
            int[] downArr = new int[fieldWidth];
            int[] leftArr = new int[fieldHigh];
            int[] rightArr = new int[fieldHigh];
            initArr(upArr);
            initArr(downArr);
            initArr(leftArr);
            initArr(rightArr);
            var rand = new Random();
            for (int i = 0; i < amountExperiments; i++)
            {
                curPos = new Point(Decimal.ToInt32(firstPosUpDown1.Value), Decimal.ToInt32(firstPosUpDown2.Value));
                while (true)
                {
                    double res = rand.NextDouble();
                    if (res < upProbability)
                    {
                        curPos.Y--;
                        if (curPos.Y <= 0)
                        {
                            upAmount++;
                            upArr[curPos.X]++;
                            break;
                        }
                    }
                    else if (res > upProbability && res < upProbability + downProbability)
                    {
                        curPos.Y++;
                        if (curPos.Y >= fieldHigh)
                        {
                            downAmount++;
                            downArr[curPos.X]++;
                            break;
                        }
                    }
                    else if (res > upProbability + downProbability && res < upProbability + downProbability + rightProbability)
                    {
                        curPos.X++;
                        if (curPos.X >= fieldWidth)
                        {
                            rightAmount++;
                            rightArr[curPos.Y]++;
                            break;
                        }
                    }
                    else if (res > upProbability + downProbability + rightProbability && res < upProbability + downProbability + rightProbability + leftProbability)
                    {
                        curPos.X--;
                        if (curPos.X <= 0)
                        {
                            leftAmount++;
                            leftArr[curPos.Y]++;
                            break;
                        }
                    }
                    else
                    {
                        stopAmount++;
                        stopArr[curPos.Y][curPos.X]++;
                        break;
                    }
                }

            }
            double upPosib = (double)upAmount / amountExperiments;
            double downPosib = (double)downAmount / amountExperiments;
            double rightPosib = (double)rightAmount / amountExperiments;
            double leftPosib = (double)leftAmount / amountExperiments;
            double stopPosib = (double)stopAmount / amountExperiments;
            resUpPosib.Text = "Up " + upPosib + " UNS: "+Math.Sqrt(upPosib*(1-upPosib)/amountExperiments);
            downResPos.Text = "Down " + downPosib + " UNS: " + Math.Sqrt(downPosib * (1 - downPosib) / amountExperiments);
            rightResPos.Text =  "Right " + rightPosib + " UNS: " + Math.Sqrt(rightPosib * (1 - rightPosib) / amountExperiments);
            leftResPos.Text = "Left " + leftPosib + " UNS: " + Math.Sqrt(leftPosib * (1 - leftPosib) / amountExperiments);
            stopResPos.Text = "Stop " + stopPosib + " UNS: " + Math.Sqrt(stopPosib * (1 - stopPosib) / amountExperiments);
            
            var series1 = chart1.Series["Up"];
            fillChart(upArr, amountExperiments, series1);
            var series2 = chart1.Series["Down"];
            fillChart(downArr, amountExperiments, series2);
            var series3 = chart2.Series["Left"];
            fillChart(leftArr, amountExperiments, series3);
            var series4 = chart2.Series["Right"];
            fillChart(rightArr, amountExperiments, series4);


            chart3.ChartAreas[0].Area3DStyle.Enable3D = true;
            for(int i = 0; i < fieldHigh; i++)
            {
                Series stopSeries = new Series((i + 1).ToString());
                for(int j = 0; j<fieldWidth; j++)
                {
                    if(stopArr[i][j] != 0)
                    {
                        double normalized = (double)stopArr[i][j] / amountExperiments;
                        stopSeries.Points.AddXY(j, normalized);
                    }
                }
                chart3.Series.Insert(0, stopSeries);
            }
            button1.Enabled = false;
            button2.Enabled = true;
        }

        private void fillChart(int[] arr, int iterations, Series series)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                series.Points.AddXY(i, arr[i] / (double)iterations);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            init();
            button1.Enabled = true;
            button2.Enabled = false;

        }
    }
}
