﻿
namespace Lab2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.widthUpDown = new System.Windows.Forms.NumericUpDown();
            this.highUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.firstPosUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.firstPosUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.AmountExpUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUp = new System.Windows.Forms.NumericUpDown();
            this.numericDown = new System.Windows.Forms.NumericUpDown();
            this.numericLeft = new System.Windows.Forms.NumericUpDown();
            this.numericRight = new System.Windows.Forms.NumericUpDown();
            this.numericStop = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.resUpPosib = new System.Windows.Forms.Label();
            this.downResPos = new System.Windows.Forms.Label();
            this.leftResPos = new System.Windows.Forms.Label();
            this.rightResPos = new System.Windows.Forms.Label();
            this.stopResPos = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.widthUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.highUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstPosUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstPosUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountExpUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            this.SuspendLayout();
            // 
            // widthUpDown
            // 
            this.widthUpDown.Location = new System.Drawing.Point(12, 29);
            this.widthUpDown.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.widthUpDown.Name = "widthUpDown";
            this.widthUpDown.Size = new System.Drawing.Size(120, 22);
            this.widthUpDown.TabIndex = 0;
            // 
            // highUpDown
            // 
            this.highUpDown.Location = new System.Drawing.Point(170, 29);
            this.highUpDown.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.highUpDown.Name = "highUpDown";
            this.highUpDown.Size = new System.Drawing.Size(120, 22);
            this.highUpDown.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Width";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(181, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Heigth";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(308, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Default position";
            // 
            // firstPosUpDown1
            // 
            this.firstPosUpDown1.Location = new System.Drawing.Point(454, 29);
            this.firstPosUpDown1.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.firstPosUpDown1.Name = "firstPosUpDown1";
            this.firstPosUpDown1.Size = new System.Drawing.Size(120, 22);
            this.firstPosUpDown1.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Number of experiments";
            // 
            // firstPosUpDown2
            // 
            this.firstPosUpDown2.Location = new System.Drawing.Point(609, 29);
            this.firstPosUpDown2.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.firstPosUpDown2.Name = "firstPosUpDown2";
            this.firstPosUpDown2.Size = new System.Drawing.Size(120, 22);
            this.firstPosUpDown2.TabIndex = 7;
            // 
            // AmountExpUpDown
            // 
            this.AmountExpUpDown.Location = new System.Drawing.Point(229, 73);
            this.AmountExpUpDown.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.AmountExpUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.AmountExpUpDown.Name = "AmountExpUpDown";
            this.AmountExpUpDown.Size = new System.Drawing.Size(120, 22);
            this.AmountExpUpDown.TabIndex = 8;
            this.AmountExpUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Up";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Down";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(226, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 17);
            this.label7.TabIndex = 11;
            this.label7.Text = "Left";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(226, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 17);
            this.label8.TabIndex = 12;
            this.label8.Text = "Right";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(440, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 17);
            this.label9.TabIndex = 13;
            this.label9.Text = "Stop";
            // 
            // numericUp
            // 
            this.numericUp.DecimalPlaces = 4;
            this.numericUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericUp.Location = new System.Drawing.Point(70, 108);
            this.numericUp.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUp.Name = "numericUp";
            this.numericUp.Size = new System.Drawing.Size(120, 22);
            this.numericUp.TabIndex = 14;
            // 
            // numericDown
            // 
            this.numericDown.DecimalPlaces = 4;
            this.numericDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericDown.Location = new System.Drawing.Point(70, 141);
            this.numericDown.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericDown.Name = "numericDown";
            this.numericDown.Size = new System.Drawing.Size(120, 22);
            this.numericDown.TabIndex = 15;
            // 
            // numericLeft
            // 
            this.numericLeft.DecimalPlaces = 4;
            this.numericLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericLeft.Location = new System.Drawing.Point(285, 106);
            this.numericLeft.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericLeft.Name = "numericLeft";
            this.numericLeft.Size = new System.Drawing.Size(120, 22);
            this.numericLeft.TabIndex = 16;
            // 
            // numericRight
            // 
            this.numericRight.DecimalPlaces = 4;
            this.numericRight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericRight.Location = new System.Drawing.Point(285, 145);
            this.numericRight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericRight.Name = "numericRight";
            this.numericRight.Size = new System.Drawing.Size(120, 22);
            this.numericRight.TabIndex = 17;
            // 
            // numericStop
            // 
            this.numericStop.DecimalPlaces = 4;
            this.numericStop.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericStop.Location = new System.Drawing.Point(534, 126);
            this.numericStop.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericStop.Name = "numericStop";
            this.numericStop.Size = new System.Drawing.Size(120, 22);
            this.numericStop.TabIndex = 18;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(229, 194);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // resUpPosib
            // 
            this.resUpPosib.AutoSize = true;
            this.resUpPosib.Location = new System.Drawing.Point(9, 234);
            this.resUpPosib.Name = "resUpPosib";
            this.resUpPosib.Size = new System.Drawing.Size(26, 17);
            this.resUpPosib.TabIndex = 20;
            this.resUpPosib.Text = "Up";
            // 
            // downResPos
            // 
            this.downResPos.AutoSize = true;
            this.downResPos.Location = new System.Drawing.Point(9, 265);
            this.downResPos.Name = "downResPos";
            this.downResPos.Size = new System.Drawing.Size(43, 17);
            this.downResPos.TabIndex = 21;
            this.downResPos.Text = "Down";
            // 
            // leftResPos
            // 
            this.leftResPos.AutoSize = true;
            this.leftResPos.Location = new System.Drawing.Point(399, 234);
            this.leftResPos.Name = "leftResPos";
            this.leftResPos.Size = new System.Drawing.Size(32, 17);
            this.leftResPos.TabIndex = 22;
            this.leftResPos.Text = "Left";
            // 
            // rightResPos
            // 
            this.rightResPos.AutoSize = true;
            this.rightResPos.Location = new System.Drawing.Point(399, 265);
            this.rightResPos.Name = "rightResPos";
            this.rightResPos.Size = new System.Drawing.Size(41, 17);
            this.rightResPos.TabIndex = 23;
            this.rightResPos.Text = "Right";
            // 
            // stopResPos
            // 
            this.stopResPos.AutoSize = true;
            this.stopResPos.Location = new System.Drawing.Point(9, 298);
            this.stopResPos.Name = "stopResPos";
            this.stopResPos.Size = new System.Drawing.Size(37, 17);
            this.stopResPos.TabIndex = 24;
            this.stopResPos.Text = "Stop";
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(893, 9);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "High";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Down";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(639, 370);
            this.chart1.TabIndex = 25;
            this.chart1.Text = "chart1";
            // 
            // chart2
            // 
            chartArea2.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart2.Legends.Add(legend2);
            this.chart2.Location = new System.Drawing.Point(893, 406);
            this.chart2.Name = "chart2";
            this.chart2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Left";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Right";
            this.chart2.Series.Add(series3);
            this.chart2.Series.Add(series4);
            this.chart2.Size = new System.Drawing.Size(639, 370);
            this.chart2.TabIndex = 26;
            this.chart2.Text = "chart2";
            // 
            // chart3
            // 
            chartArea3.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart3.Legends.Add(legend3);
            this.chart3.Location = new System.Drawing.Point(14, 330);
            this.chart3.Name = "chart3";
            this.chart3.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            this.chart3.Series.Add(series5);
            this.chart3.Size = new System.Drawing.Size(736, 446);
            this.chart3.TabIndex = 27;
            this.chart3.Text = "chart3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(502, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 17);
            this.label10.TabIndex = 28;
            this.label10.Text = "X";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(655, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 17);
            this.label11.TabIndex = 29;
            this.label11.Text = "Y";
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(433, 194);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(105, 23);
            this.button2.TabIndex = 30;
            this.button2.Text = "Stop counting";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1531, 774);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.chart3);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.stopResPos);
            this.Controls.Add(this.rightResPos);
            this.Controls.Add(this.leftResPos);
            this.Controls.Add(this.downResPos);
            this.Controls.Add(this.resUpPosib);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.numericStop);
            this.Controls.Add(this.numericRight);
            this.Controls.Add(this.numericLeft);
            this.Controls.Add(this.numericDown);
            this.Controls.Add(this.numericUp);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AmountExpUpDown);
            this.Controls.Add(this.firstPosUpDown2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.firstPosUpDown1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.highUpDown);
            this.Controls.Add(this.widthUpDown);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.widthUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.highUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstPosUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstPosUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountExpUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown widthUpDown;
        private System.Windows.Forms.NumericUpDown highUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown firstPosUpDown1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown firstPosUpDown2;
        private System.Windows.Forms.NumericUpDown AmountExpUpDown;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUp;
        private System.Windows.Forms.NumericUpDown numericDown;
        private System.Windows.Forms.NumericUpDown numericLeft;
        private System.Windows.Forms.NumericUpDown numericRight;
        private System.Windows.Forms.NumericUpDown numericStop;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label resUpPosib;
        private System.Windows.Forms.Label downResPos;
        private System.Windows.Forms.Label leftResPos;
        private System.Windows.Forms.Label rightResPos;
        private System.Windows.Forms.Label stopResPos;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button2;
    }
}

