package com.interpak.smarthome.knx.database.dao;

public class GrDAOSpec {
	public static final String UNIVERSAL_PROPERTY_TYPE = "type";
	
	// user properties
	public static final String USER_CLASS 					= "user";
	public static final String USER_PROPERTY_FIRST_NAME 	= "firstName";
	public static final String USER_PROPERTY_LAST_NAME 		= "lastName";
	public static final String USER_PROPERTY_EMAIL 			= "email";
	public static final String USER_PROPERTY_PASSWORD		= "password";
	public static final String USER_PROPERTY_TOKEN			= "token";
	public static final String USER_PROPERTY_ROLE			= "role";
}
