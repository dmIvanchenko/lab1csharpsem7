package com.interpak.smarthome.knx.restapi;

import com.interpak.smarthome.knx.filter.AuthenticationFilter;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *  set the filter applications manually and not via web.xml
 */
public class RestApplicationConfig extends ResourceConfig {
	
	public RestApplicationConfig() {
        packages( "com.interpak.smarthome.knx.filter" );
		register( AuthenticationFilter.class );
	}
}
