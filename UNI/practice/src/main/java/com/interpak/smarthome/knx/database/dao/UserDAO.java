package com.interpak.smarthome.knx.database.dao;

import java.util.List;

import com.interpak.smarthome.knx.exception.UserExistingException;
import com.interpak.smarthome.knx.model.User;
import com.interpak.smarthome.knx.model.UserSecurity;
import com.interpak.smarthome.knx.exception.UserNotFoundException;

public interface UserDAO {
	public boolean createUser( UserSecurity user ) throws UserExistingException;
	
	public String getUserIdByEmail( String email ) throws UserNotFoundException;
	public User getUser(String id ) throws UserNotFoundException;
	
	public List<User> getAllUsers();
	
	public UserSecurity getUserAuthentication( String id ) throws UserNotFoundException;
	public boolean setUserAuthentication( UserSecurity user ) throws UserNotFoundException;
	
	public boolean updateUser( User user ) throws UserNotFoundException;
	public boolean deleteUser( String id ) throws UserNotFoundException;
}
