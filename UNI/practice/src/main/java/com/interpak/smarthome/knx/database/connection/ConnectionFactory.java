package com.interpak.smarthome.knx.database.connection;

import com.interpak.smarthome.knx.config.DbConfig;
import com.interpak.smarthome.knx.database.connection.sqlite.SQLiteConnection;

public class ConnectionFactory {
	
	private static Connection connection = null;
	
	public static Connection getConnection() {
		if( connection != null ) return connection;
		
		switch( DbConfig.getDbType() ) {

			case SQLITE:
				connection = new SQLiteConnection();
			default:
				break;
		}

		// open connection
		connection.open();
		
		return connection;
	}
}
