package com.interpak.smarthome.knx.database.def;

public enum DataBaseType {

	SQLITE("SQLITE");
	
    private final String val;       

    private DataBaseType( String s ) {
    	val = s;
    }
    
    public String toString() {
       return this.val;
    }
}
