package com.interpak.smarthome.knx.database.dao;

import com.interpak.smarthome.knx.config.DbConfig;
import com.interpak.smarthome.knx.database.connection.ConnectionFactory;
import com.interpak.smarthome.knx.database.connection.Connection;
import com.interpak.smarthome.knx.database.dao.sqlite.SqlUserDAO;

public class UserDAOFactory {
	
	public static UserDAO getUserDAO() {

		Connection connection = ConnectionFactory.getConnection();

		switch( DbConfig.getDbType() ) {

			case SQLITE:
				return new SqlUserDAO( connection );
			default:
				return null;
		}
	}
}
